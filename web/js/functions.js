function searchOrder() {
    var trackNo = $('#track_no').val();
    $('#result').html('');
    if ($('#result').css('display') != 'none') {
        $('#result').hide();
    }

    if (trackNo == '') {
        alert('Please insert a track code');
        return;
    }

    $.get( "rest/track_shipment?track_no="+trackNo, function( data ) {
        var result = JSON.parse(data);
        if (result.success == true) {
            var spDate = $.format.date(result.data.shipping_date, 'dd-mm-yyyy');
            $('#result').html('Your order will be delivered on ' + spDate + ' between 9 AM and 5 PM.');
            $('#result').show();
        } else {
            alert(result.message);
        }
    });
}