<?php
namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="psh_order")
 */
class CustomerOrder
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"comment":"id"})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_on", type="datetime", nullable=true)
     */
    protected $updatedOn;

    /**
     * @var string
     * @ORM\Column(name="updated_by", type="string", length=64, nullable=true)
     */
    protected $updatedBy;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_on", type="datetime", nullable=true)
     */
    protected $createdOn;

    /**
     * @var string
     * @ORM\Column(name="created_by", type="string", length=64, nullable=true)
     */
    protected $createdBy;

    /**
     * @var string
     * @ORM\Column(name="orderNo", type="string", length=64, nullable=false)
     */
    protected $orderNo;

    /**
     * @var string
     * @ORM\Column(name="track_code", type="string", length=64, nullable=true)
     */
    protected $trackCode;

    /**
     * @var \DateTime
     * @ORM\Column(name="shipping_date", type="datetime", nullable=true)
     */
    protected $shippingDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param \DateTime $updatedOn
     * @return $this
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param string $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return $this
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrackCode()
    {
        return $this->trackCode;
    }

    /**
     * @param string $trackCode
     */
    public function setTrackCode($trackCode)
    {
        $this->trackCode = $trackCode;
    }

    /**
     * @return string
     */
    public function getOrderNo()
    {
        return $this->orderNo;
    }

    /**
     * @param string $orderNo
     */
    public function setOrderNo($orderNo)
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @return \DateTime
     */
    public function getShippingDate()
    {
        return $this->shippingDate;
    }

    /**
     * @param \DateTime $shippingDate
     */
    public function setShippingDate($shippingDate)
    {
        $this->shippingDate = $shippingDate;
    }
}