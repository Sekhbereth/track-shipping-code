<?php
namespace BackendBundle\Controller;

use BackendBundle\Entity\CustomerOrder;
use Doctrine\DBAL\Exception\ConnectionException;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;


class TrackShipmentController extends FOSRestController
{
    /**
     * @Rest\Get("/track_shipment")
     * @param Request $request
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function getShipmentDate(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('jms_serializer');

        try {
            //verify params
            $trackNo = $request->query->get('track_no', null);
            if (empty($trackNo)) {
                return new Response($serializer->serialize(array('success' => false, 'message' => 'Please fill in the track no.'), 'json'));
            }

            //get entity manager
            try {
                $em = $this->getDoctrine()->getManager();
                $order = $em->getRepository(CustomerOrder::class)->findOneBy(array('trackCode' => $trackNo));
                if (empty($order)) {
                    return new Response($serializer->serialize(array('success' => false, 'message' => 'There was no order found for given track no: '.$trackNo), 'json'));
                }
            } catch(ConnectionException $e){
                $liteEm = $this->getDoctrine()->getManager('sqlite');
                $order = $liteEm->getRepository(CustomerOrder::class)->findOneBy(array('trackCode' => $trackNo));
                if (empty($order)) {
                    return new Response($serializer->serialize(array('success' => false, 'message' => 'There was no order found for given track no: '.$trackNo), 'json'));
                }
            }

            return new Response($serializer->serialize(array('success' => true, 'data' => $order), 'json'));

        } catch (\Exception $e) {
            return new Response($serializer->serialize(array('success' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()), 'json'));
        }
    }
}