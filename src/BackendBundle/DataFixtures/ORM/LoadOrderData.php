<?php

namespace BackendBundle\DataFixtures\ORM;

use BackendBundle\Entity\CustomerOrder;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOrderData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     * call with php bin\console doctrine:fixtures:load --fixtures=src/BackendBundle/DataFixtures/ORM/LoadOrderData.php --append
     * add --em=sqlite for the second dba
     */
    public function load(ObjectManager $manager)
    {
        $base = new CustomerOrder();
        $base->setCreatedBy('init')->setCreatedOn(new \DateTime('now'))->setUpdatedBy('init')
            ->setUpdatedOn(new \DateTime('now'));

        for ($i = 1; $i <= 10; $i++) {
            $token = md5('order'.$i);
            $order = clone $base;
            $order->setOrderNo($i);
            $order->setTrackCode($token);
            $order->setShippingDate(new \DateTime('now'));
            $manager->persist($order);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}